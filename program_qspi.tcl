# Company: IPFN-IST
# Engineer:  B. Carvalho
#
# Create Date:    10:15:58 08/09/2017
#
# Project Name:   TE041 FPGA module dual blinky
# Target Devices: Kintex xc7k325tfbg676-2 Tool versions:  Vivado 2016.4
# 
# program_qspi.tcl: Tcl script 
#
# Script to program SPI flash on Trenz TE041 FPGA modules. 
# 
#Start by :
# Open Hardware Manager
# Connect Target
#On TCL console
#open_hw
#connect_hw_server -url localhost:3121
#refresh_hw_server {localhost:3121}
#open_hw_target 
#
#source ... /program_qspi.tcl
# $URL: http://metis.ipfn.ist.utl.pt:8888/svn/cdaq/INSTRUMENTATION/1U_Data_Aquisition/Firmware/Vivado-16.4/mercury-kx1/program_qspi.tcl $
#

# https://www.xilinx.com/support/answers/61067.html
#https://forums.xilinx.com/t5/Configuration/program-hw-cfgmem-and-Quad-IO-Enable/td-p/520359
#https://forum.trenz-electronic.de/index.php?topic=718.0

# Set the reference directory to where the script is

set origin_dir [file dirname [info script]]
#set origin_dir "/TE0741/blinky-dual"
puts "origin_dir: $origin_dir "

#SPIx4
write_cfgmem -force -format MCS -size 256 -interface SPIx4 -loadbit "up 0x0  $origin_dir/blinky_project/blinky_project.runs/impl_1/blinky_dual.bit" "$origin_dir/program_files/blinky_dual.mcs"

create_hw_cfgmem -hw_device [lindex [get_hw_devices] 0] -mem_dev [lindex [get_cfgmem_parts {s25fl256sxxxxxx0-spi-x1_x2_x4}] 0]

set_property PROGRAM.FILES [list "$origin_dir/program_files/blinky_dual.mcs"] [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0]]
set_property PROGRAM.ADDRESS_RANGE  {use_file} [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
set_property PROGRAM.UNUSED_PIN_TERMINATION {pull-none} [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
set_property PROGRAM.BLANK_CHECK  0 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
set_property PROGRAM.ERASE  1 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
set_property PROGRAM.CFG_PROGRAM  1 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
set_property PROGRAM.VERIFY  1 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
set_property PROGRAM.CHECKSUM  0 [ get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]
startgroup 
if {![string equal [get_property PROGRAM.HW_CFGMEM_TYPE  [lindex [get_hw_devices] 0]] [get_property MEM_TYPE [get_property CFGMEM_PART [get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]]]] }  { create_hw_bitstream -hw_device [lindex [get_hw_devices] 0] [get_property PROGRAM.HW_CFGMEM_BITFILE [ lindex [get_hw_devices] 0]]; program_hw_devices [lindex [get_hw_devices] 0]; }; 
#INFO: [Labtools 27-3164] End of startup status: HIGH
program_hw_cfgmem -hw_cfgmem [get_property PROGRAM.HW_CFGMEM [lindex [get_hw_devices] 0 ]]

endgroup
#INFO: [Labtools 27-3164] End of startup status: HIGH

boot_hw_device  [lindex [get_hw_devices] 0]

