`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: IPFN-IST
// Engineer: B. Carvalho
//  
// Create Date:    Tue Jan 30 12:54:31 WET 2018
// Design Name:    fpga-te0741-blinky
// Module Name:    blinky_dual 
// Project Name:   
// Target Devices: Kintex xc7k325tfbg676-2
// Tool versions:  Vivado 2016.4
// Description: 
//
// Dependencies: 
//
//
// Copyright 2013 - 2016 IPFN-Instituto Superior Tecnico, Portugal
// 
// Licensed under the EUPL, Version 1.1 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:

// http://ec.europa.eu/idabc/eupl

// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//////////////////////////////////////////////////////////////////////////////////
module blinky_dual
    (
    input 			CLK_MGT_P,		
    input           CLK_MGT_N,        

    input 			CLK_P,		
    input           CLK_N,        

    output           EN_CLK,        
    
    output           LED1,        
    output           LED2
);       

    wire mgtclk, clk;
    reg [31:0] cnt = 0;
    reg [31:0] mcnt = 0;
    
    assign EN_CLK = 1'b1;  

    assign LED1 = cnt[26];  // Red LED
    assign LED2 = mcnt[28]; // Green LED 

  IBUFDS_GTE2 #(
      .CLKCM_CFG("TRUE"),   // Refer to Transceiver User Guide
      .CLKRCV_TRST("TRUE"),//, // Refer to Transceiver User Guide
      .CLKSWING_CFG(2'b11)  // This attribute must always be set to 2'b11. (ug482_7Series_GTP_Transceivers)
   )
   IBUFDS_GTE2_inst (
      .O(mgtclk),         // 1-bit output: Refer to Transceiver User Guide
      //.ODIV2(ODIV2), // 1-bit output: Refer to Transceiver User Guide
      .CEB(1'b0),     // 1-bit input: Refer to Transceiver User Guide
      .I(CLK_MGT_P),         // 1-bit input: Refer to Transceiver User Guide
      .IB(CLK_MGT_N)        // 1-bit input: Refer to Transceiver User Guide
   );

   IBUFDS #(
      .DIFF_TERM("FALSE"),       // Differential Termination
      .IBUF_LOW_PWR("TRUE") //,     // Low power="TRUE", Highest performance="FALSE" 
      //.IOSTANDARD("DEFAULT")     // Specify the input I/O standard
   ) IBUFDS_inst (
      .O(clk),  // Buffer output
      .I(CLK_P),  // Diff_p buffer input (connect directly to top-level port)
      .IB(CLK_N) // Diff_n buffer input (connect directly to top-level port)
   );

    always @( posedge clk )
        begin
	      cnt <= cnt + 1'b1;            
 	    end       

    always @( posedge mgtclk )
        begin
	      mcnt <= mcnt + 1'b1;            
 	    end       

endmodule
