###############################################################################
# Pinout and Related I/O Constraints
###############################################################################
# Company: IPFN-IST
# Engineer: B. Carvalho
#  
# Create Date:    Mon 29 Jan 11:34:46 WET 2018
# Design Name: 
# Module Name:    blinky  for Trenz TE0741
# Project Name: 	
# Target Devices: Kintex xc7k325tfbg676-2 
# Tool versions:  Vivado 2016.4
# Description: 
#
# Dependencies: 
#
# Revision: 
# Revision 0.01 - File Created
# Additional Comments: 
# 
# https://wiki.trenz-electronic.de/display/TE0741/LED

create_clock -name mgtclk125 -period 8.5 [get_ports CLK_MGT_P]

set_property LOC F6 [get_ports CLK_MGT_P]
set_property LOC F5 [get_ports CLK_MGT_N]

#Critical warnning [Vivado 12-1815] Setting property 'IOSTANDARD' is not allowed for GT terminals.
#set_property IOSTANDARD LVDS_25 [get_ports CLK_MGT_P]
#set_property IOSTANDARD LVDS_25 [get_ports CLK_MGT_N]

create_clock -name clk100 -period 10.0 [get_ports CLK_P]

# bank 14 was LVCMOS33  !!
set_property LOC F22 [get_ports CLK_P]
set_property LOC E23 [get_ports CLK_N]
set_property IOSTANDARD LVDS_25 [get_ports CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports CLK_N]

set_property IOSTANDARD LVCMOS33 [get_ports EN_CLK]
set_property LOC C26 [get_ports EN_CLK]

# bank 14
set_property PACKAGE_PIN D26 [get_ports LED1]
set_property IOSTANDARD LVCMOS33 [get_ports LED1]
set_property PACKAGE_PIN E26 [get_ports LED2]
set_property IOSTANDARD LVCMOS33 [get_ports LED2]

#Bank 0 3.3 V
#set_property CONFIG_VOLTAGE 3.3 [current_design]

# bit_common.xdc
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.M1PIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.M2PIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.M0PIN PULLNONE [current_design]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

